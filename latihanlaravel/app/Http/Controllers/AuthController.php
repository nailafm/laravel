<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
         // dd($request->all());
      $name = $request['name'];
      $lastname = $request['lastname'];

        return view('halaman.welcome',compact('name','lastname'));
    }
}