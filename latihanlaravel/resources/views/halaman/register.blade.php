@extends('layout.master')

@section('judul')
Buat Account Baru!   
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        {{csrf_field()}}
        <label for="name">First name:</label> <br><br>
        <input type="text" id="name"> <br><br>
        <label for="lastname">Last name:</label> <br><br>
        <input type="text" id="lastname"> <br><br>
        <label for="gender">Gender:</label> <br><br>
        <input type="radio" name="e" value="Male"> Male <br>
        <input type="radio" name="e" value="Female"> Female <br>
        <input type="radio" name="e" value="Other"> Other <br> <br>
        <label for="Nationality">Nationality:</label> <br><br>
        <select name="Nationality" id="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select> <br><br>
        <label for="Language Spoken">Language Spoken:</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label for="Bio">Bio:</label> <br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up"> 

    </form>
@endsection